package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
)

// TODO: Add indexes
var databaseTables = map[string][]string{
	"accounts": {
		"`key` VARCHAR(145) NOT NULL DEFAULT ''",
		"`nickname` VARCHAR(200) NOT NULL DEFAULT ''",
	},
	"draws": {
		"`date` VARCHAR(10) NOT NULL DEFAULT ''",
		"`number` INTEGER NOT NULL DEFAULT ''",
	},
	"predictions": {
		"`user` VARCHAR(145) NOT NULL DEFAULT ''",
		"`date` VARCHAR(10) NOT NULL DEFAULT ''",
		"`number` INTEGER NOT NULL DEFAULT ''",
	},
}

var pacificTime *time.Location

type predictionResult struct {
	Number      int
	Predictions []int
}

type draw struct {
	Number int
}

type prediction struct {
	Number int
}

type database struct {
	*sql.DB
}

func newDatabase(driver string, dataSource string) (*database, error) {
	var err error
	pacificTime, err = time.LoadLocation("America/Los_Angeles")
	if err != nil {
		return nil, err
	}

	d, err := sql.Open(driver, dataSource)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %s", err)
	}

	_, err = d.Exec(`PRAGMA encoding="UTF-8"`)
	if err != nil {
		return nil, fmt.Errorf("failed to send PRAGMA: %s", err)
	}

	db := &database{d}

	err = db.CreateTables()
	if err != nil {
		_ = db.Close()
		return nil, fmt.Errorf("failed to create tables: %s", err)
	}

	return db, nil
}

func (d *database) CreateTables() error {
	var (
		tcolumns string
		err      error
	)

	for tname, tcols := range databaseTables {
		tcolumns = strings.Join(tcols, ",")
		_, err = d.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (%s)", tname, tcolumns))
		if err != nil {
			return fmt.Errorf("failed to create table %s: %s", tname, err)
		}
	}

	return nil
}

func (d *database) addPrediction(date time.Time, user string) error {
	if !isTomorrow(date) {
		return errors.New("prediction is not for tomorrow")
	}

	_, err := d.Exec("DELETE FROM `predictions` WHERE `date` = ? AND `user` = ?", date.Format("2006-01-02"), user)
	if err != nil {
		return err
	}

	_, err = d.Exec("INSERT INTO `predictions` VALUES (?, ?)", date.Format("2006-01-02"), user)
	if err != nil {
		return err
	}

	return nil
}

func (d *database) getPredictions(date time.Time) (*predictionResult, error) {
	predictionResult := &predictionResult{
		Predictions: make([]int, 10),
	}

	rows, err := d.Query("SELECT `number` FROM `draws` WHERE `date` = ?", date.Format("2006-01-02"))
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var d *draw
		err = rows.Scan(d)
		if err != nil {
			return nil, err
		}

		predictionResult.Number = d.Number
	}

	rows, err = d.Query("SELECT `number` FROM `predictions` WHERE `date` = ?", date.Format("2006-01-02"))
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var prediction *prediction
		err = rows.Scan(prediction)
		if err != nil {
			return nil, err
		}

		predictionResult.Predictions[prediction.Number-1]++
	}

	return predictionResult, nil
}

func isTomorrow(date time.Time) bool {
	tomorrow := time.Now().In(pacificTime).AddDate(0, 0, 1)
	y, m, d := date.Date()
	ty, tm, td := tomorrow.Date()
	return y == ty && m == tm && d == td
}
