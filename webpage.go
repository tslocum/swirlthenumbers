package main

import (
	"bytes"
	"fmt"
	"log"
	"time"
)

func pageHeader(title string) []byte {
	return []byte(`<!DOCTYPE HTML>
<html>
<head>
<title>` + title + `</title>
</head>
<body>
`)
}

func pageFooter() []byte {
	return []byte(`</body>
</html>`)
}

func predictionsPage(date time.Time) []byte {
	var b bytes.Buffer

	b.Write(pageHeader("Swirl the Numbers"))

	//b.WriteString(<ul class="holo-list">")

	// TODO invalid future or too far past date

	predictionResult, err := db.getPredictions(date)
	if err != nil {
		log.Fatal(err)
	}

	b.WriteString(fmt.Sprintf("Num %d Pred %+v", predictionResult.Number, predictionResult.Predictions))

	b.Write(pageFooter())

	return b.Bytes()
}
