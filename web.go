package main

import (
	"log"
	"net/http"
	"time"
)

func initWeb() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write(predictionsPage(time.Now().In(pacificTime)))
	})

}

func listenWeb(address string) {
	log.Fatal(http.ListenAndServe(address, nil))
}
