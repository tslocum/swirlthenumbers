package main

import "log"

var db *database

func main() {
	var err error
	db, err = newDatabase("sqlite3", "/home/trevor/programming/swirlthenumbers.db")
	if err != nil {
		log.Fatal(err)
	}

	initWeb()
	listenWeb("localhost:8080") // TODO
}
