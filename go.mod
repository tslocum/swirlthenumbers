module gitlab.com/tslocum/swirlthenumbers

go 1.15

require (
	github.com/mattn/go-sqlite3 v1.14.4
	github.com/pkg/errors v0.9.1
)
