# swirlthenumbers
[![CI status](https://gitlab.com/tslocum/swirlthenumbers/badges/master/pipeline.svg)](https://gitlab.com/tslocum/swirlthenumbers/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

Prediction tracking for [**swirlthenumbers.com**](swirlthenumbers.com)

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/swirlthenumbers/issues).
